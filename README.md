You can use the [multvardiv](https://forgemia.inra.fr/imhorphen/multvardiv) package,
which provides the same tools for the multivariate generalized Gaussian distribution
and other multvariate distributions.

# mggd

This package provides tools for multivariate generalized Gaussian distributions (MGGD):

* Calculation of distance/divergence between multivariate generalised Gaussian distributions (MGGD):
    + Kullback divergence
* Manipulation of MGGD distributions:
    + Probability density
    + Estimation of the parameters
    + Simulation of samples from a MGGD
    + Plot of the density of a MGGD with 2 variables

## Installation

Install the package from CRAN:

```
install.packages("mggd")
```

Or the development version from the repository, using the [`devtools`](https://github.com/r-lib/devtools) package:
```
install.packages("devtools")
devtools::install_git("https://forgemia.inra.fr/imhorphen/mggd")
```

## Authors

[Pierre Santagostini](mailto:pierre.santagostini@agrocampus-ouest.fr) and [Nizar Bouhlel](mailto:nizar.bouhlel@agrocampus-ouest.fr)

## Reference

N. Bouhlel, A. Dziri,
Kullback-Leibler Divergence Between Multivariate Generalized Gaussian Distributions.
IEEE Signal Processing Letters, vol. 26 no. 7, July 2019.
[https://doi.org/10.1109/LSP.2019.2915000](https://doi.org/10.1109/LSP.2019.2915000)
